# Keycloak custom theming

there are two themes available:

1) **Base theme** - this is already in Keycloak. This Thme folder should not be changed here.
2) **Custom theme**  - this must be tuned and copied to Keycloak instance.

In case you want extend something, just copy file from base theme to costom theme and it will be overrrided.