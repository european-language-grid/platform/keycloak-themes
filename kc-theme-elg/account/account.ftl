<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row">
        <div class="col-md-8">
            <h2>${msg("editAccountHtmlTitle")}</h2>
        </div>
        <div class="col-md-3 mt-4">
            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
        </div>
    </div>

    <form action="${url.accountUrl}" class="${properties.kcFormClass!}" method="post">

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

        <#if !realm.registrationEmailAsUsername>
            <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('username','has-error')}">
                <div class="${properties.kcLabelWrapperClass!}" >
                    <label for="username" class="${properties.kcLabelClass!}" >${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
                </div>

                <div class="${properties.kcInputWrapperClass!}" >
                    <input type="text" class="${properties.kcInputClass!}"  id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>
                </div>
            </div>
        </#if>

        <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('email','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
            <label for="email" class="${properties.kcLabelClass!}" >${msg("email")}</label> <span class="required">*</span>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="email" name="email" autofocus value="${(account.email!'')}"/>
            </div>
        </div>

        <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('firstName','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="firstName" class="${properties.kcLabelClass!}" >${msg("firstName")}</label> <span class="required">*</span>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="firstName" name="firstName" value="${(account.firstName!'')}"/>
            </div>
        </div>

        <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="lastName" class="${properties.kcLabelClass!}" >${msg("lastName")}</label> <span class="required">*</span>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="lastName" name="lastName" value="${(account.lastName!'')}"/>
            </div>
        </div>
         <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('affiliation','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.affiliation" class="${properties.kcLabelClass!}" >Affiliation</label>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.affiliation" name="user.attributes.affiliation" value="${(account.attributes.affiliation!'')}"/>
            </div>
        </div>

         <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('linkedin','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.linkedin" class="${properties.kcLabelClass!}" >LinkedIn</label>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.linkedin" name="user.attributes.linkedin" value="${(account.attributes.linkedin!'')}"/>
            </div>
        </div>

         <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('website','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.website" class="${properties.kcLabelClass!}" >Personal website</label>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.website" name="user.attributes.website" value="${(account.attributes.website!'')}"/>
            </div>
        </div>

           
         <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('DBLP','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.DBLP" class="${properties.kcLabelClass!}" >DBLP page</label>
            </div>
            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.DBLP" name="user.attributes.DBLP" value="${(account.attributes.DBLP!'')}"/>
            </div>
        </div>

    <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('GoogleScholar','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.GoogleScholar" class="${properties.kcLabelClass!}" >Google Scholar</label>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.GoogleScholar" name="user.attributes.GoogleScholar" value="${(account.attributes.GoogleScholar!'')}"/>
            </div>
        </div>

               <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('otherEmails','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.otherEmails" class="${properties.kcLabelClass!}" >Additional email</label>
            </div>
            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.otherEmails" name="user.attributes.otherEmails" value="${(account.attributes.otherEmails!'')}"/>
            </div>
        </div>
  
        <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('phone','has-error')}">
                <div class="${properties.kcLabelWrapperClass!}" >
                    <label for="account.attributes.phone" class="${properties.kcLabelClass!}" >Phone number</label>
                </div>

                <div class="${properties.kcInputWrapperClass!}" >
                    <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.phone" name="user.attributes.phone" value="${(account.attributes.phone!'')}"/>
                </div>
         </div>

       <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('address','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.address" class="${properties.kcLabelClass!}" >Address</label>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.address" name="user.attributes.address" value="${(account.attributes.address!'')}"/>
            </div>
        </div>

         <div class="${properties.kcFormGroupClass!}  ${messagesPerField.printIfExists('image','has-error')}">
            <div class="${properties.kcLabelWrapperClass!}" >
                <label for="account.attributes.image" class="${properties.kcLabelClass!}" >Image url</label>
            </div>

            <div class="${properties.kcInputWrapperClass!}" >
                <input type="text" class="${properties.kcInputClass!}"  id="user.attributes.image" name="user.attributes.image" value="${(account.attributes.image!'')}"/>
            </div>
         </div>    
        
        <div class="${properties.kcFormGroupClass!} ">
            <div id="kc-form-buttons" class="col-md-10 submit">
                <div class="">                 
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                </div>
            </div>
        </div>
    </form>

</@layout.mainLayout>