<#import "template.ftl" as layout>
<@layout.mainLayout active='applications' bodyClass='applications'; section>
    <#assign thirdpartyApps = applications.applications?filter(app -> (app.client.consentRequired && app.clientScopesGranted?has_content))>
    <#assign offlineApps = applications.applications?filter(app -> app.additionalGrants?has_content)>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("applicationsHtmlTitle")}</h2>
        </div>
    </div>

    <form action="${url.applicationsUrl}" method="post">
        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">
        <input type="hidden" id="referrer" name="referrer" value="${stateChecker}">

        <h3>Third-party applications</h3>
        <#list thirdpartyApps>
        <p>These are the third-party applications that you have allowed to access your ELG account.  To revoke
          your consent for an application, use the ${msg("revoke")} button below.</p>

        <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <td>${msg("application")}</td>
                <td>${msg("grantedPermissions")}</td>
                <td>${msg("action")}</td>
              </tr>
            </thead>

            <tbody>
              <#items as application>
                <tr>
                    <td>
                        <#if application.effectiveUrl?has_content><a href="${application.effectiveUrl}"></#if>
                            <#if application.client.name?has_content>${advancedMsg(application.client.name)}<#else>${application.client.clientId}</#if>
                        <#if application.effectiveUrl?has_content></a></#if>
                    </td>

                    <td>
                        <#if application.client.consentRequired>
                            <#list application.clientScopesGranted as claim>
                                ${advancedMsg(claim)}<#if claim_has_next>, </#if>
                            </#list>
                        <#else>
                            <strong>${msg("fullAccess")}</strong>
                        </#if>
                    </td>

                    <td>
                        <button type='submit' class='${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!}' style='padding: 10px 20px; margin-top: 0' id='revoke-${application.client.clientId}' name='clientId' value="${application.client.id}">${msg("revoke")}</button>
                    </td>
                </tr>
              </#items>
            </tbody>
        </table>
        <#else>
            <p>You have not granted any third-party applications access to your ELG account.</p>
        </#list>

        <h3>Offline access tokens</h3>
        <#list offlineApps>
            <p>The following applications have been issued with tokens giving long-term "offline" access to your
              ELG account. To revoke the tokens for an application, use the ${msg("revoke")} button below.</p>
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <td>${msg("application")}</td>
                  <td>${msg("action")}</td>
                </tr>
              </thead>

              <tbody>
                <#items as application>
                  <tr>
                    <td>
                        <#if application.effectiveUrl?has_content><a href="${application.effectiveUrl}"></#if>
                            <#if application.client.name?has_content>${advancedMsg(application.client.name)}<#else>${application.client.clientId}</#if>
                            <#if application.effectiveUrl?has_content></a></#if>
                    </td>

                    <td>
                      <button type='submit' class='${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!}' style='padding: 10px 20px; margin-top: 0' id='revoke-${application.client.clientId}' name='clientId' value="${application.client.id}">${msg("revoke")}</button>
                    </td>
                  </tr>
                </#items>
              </tbody>
            </table>
          <#else>
              <p>You have not issued any offline access tokens.</p>
        </#list>
    </form>

</@layout.mainLayout>