<#import "template.ftl" as layout>
<@layout.registrationLayout bodyClass="oauth"; section>
    <#if section = "header">
        <#if (client.attributes["elg.logoUrl"])?has_content>
            <img src="${client.attributes["elg.logoUrl"]}" alt="${advancedMsg(client.name)}"/>
        </#if>
        <#if client.name?has_content>
            ${msg("oauthGrantTitle",advancedMsg(client.name))}
        <#else>
            ${msg("oauthGrantTitle",client.clientId)}
        </#if>
    <#elseif section = "form">
        <div id="kc-oauth" class="content-area">
            <p>${msg("oauthGrantDescription",advancedMsg(client.name))}</p>
            <ul>
                <#if oauth.clientScopesRequested??>
                    <#list oauth.clientScopesRequested as clientScope>
                        <li>
                            <span>${advancedMsg(clientScope.consentScreenText)}</span>
                        </li>
                    </#list>
                </#if>
            </ul>

            <#if (client.attributes["elg.privacyPolicyUrl"])?has_content || (client.attributes["elg.termsUrl"])?has_content>
                <p>
                    <#if client.name?has_content>
                        ${msg("oauthGrantInformation",advancedMsg(client.name))}
                    <#else>
                        ${msg("oauthGrantInformation",client.clientId)}
                    </#if>
                </p>
                <ul>
                    <#if (client.attributes["elg.termsUrl"])?has_content>
                        <li>
                            <a href="${client.attributes["elg.termsUrl"]}" target="_blank">${msg("oauthGrantTos")}</a>
                        </li>
                    </#if>
                    <#if (client.attributes["elg.privacyPolicyUrl"])?has_content>
                        <li>
                            <a href="${client.attributes["elg.privacyPolicyUrl"]}" target="_blank">${msg("oauthGrantPolicy")}</a>
                        </li>
                    </#if>
                </ul>
            </#if>

            <h3>${msg("oauthGrantRequest")}</h3>

            <form class="form-actions" action="${url.oauthAction}" method="POST">
                <input type="hidden" name="code" value="${oauth.code}">
                <div class="${properties.kcFormGroupClass!}">
                    <div id="kc-form-options">
                        <div class="${properties.kcFormOptionsWrapperClass!}">
                        </div>
                    </div>

                    <div id="kc-form-buttons">
                        <div class="${properties.kcFormButtonsWrapperClass!}">
                            <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="accept" id="kc-login" type="submit" value="${msg("doYes")}"/>
                            <input class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="cancel" id="kc-cancel" type="submit" value="${msg("doNo")}"/>
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </#if>
</@layout.registrationLayout>
