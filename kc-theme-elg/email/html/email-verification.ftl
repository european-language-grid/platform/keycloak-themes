<html>
<body>
<div>
    ${kcSanitize(msg("emailLogoHTML"))?no_esc}  
</div>
${kcSanitize(msg("emailVerificationBodyHtml",link, linkExpiration, realmName, linkExpirationFormatter(linkExpiration),user.getFirstName()))?no_esc}
</body>
</html>
