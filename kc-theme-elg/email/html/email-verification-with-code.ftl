<html>
<body>
<div>
    ${kcSanitize(msg("emailLogoHTML"))?no_esc}  
</div>
${kcSanitize(msg("emailVerificationBodyCodeHtml",code))?no_esc}
</body>
</html>
