<html>
<body>
<div>
    ${kcSanitize(msg("emailLogoHTML"))?no_esc}  
</div>
${kcSanitize(msg("identityProviderLinkBodyHtml", identityProviderAlias, realmName, identityProviderContext.username, link, linkExpiration, linkExpirationFormatter(linkExpiration)))?no_esc}
</body>
</html>