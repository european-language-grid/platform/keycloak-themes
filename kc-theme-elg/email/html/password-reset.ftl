<html>
<body>
<div>
    ${kcSanitize(msg("emailLogoHTML"))?no_esc}  
</div>
${kcSanitize(msg("passwordResetBodyHtml",link, linkExpiration, realmName, linkExpirationFormatter(linkExpiration)))?no_esc}
</body>
</html>